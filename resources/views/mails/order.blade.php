@extends('mails.default')
@section('content')
    <center><p style="font-size:20px; color:#2d4052;" align="center" ><b>Hey {{$name??''}},your order #{{$order->id}} has been successfully placed !</b></center>
    <center><p style="font-size:20px; color:#2d4052;" align="center" ><b>Purchase Date :- {{Date('d M,y')}}</b> </p></center>
    <center><p style="font-size:20px; color:#2d4052;" align="center" ><b>Purchase Time :- {{Date("h:i A")}}</b> </p></center>
    <center><p style="font-size:20px; color:#2d4052;" align="center" ><b>Order ID :- {{$order->id}}</b> </p></center>

    <center>
        <table>
            <tr>
                <td colspan="2"><h3  style="color:#36ade2;  text-align:center;  background-color:#e6ebf1; border-radius:10px; font-size:18px; font-weight:700; width:400px;  padding:10px;">ORDER</h3></td>
            </tr>
            
            <tr>
                <td><b style="font-size:18px; color:#36ade2;">Mode Of Payment</b></td>
                <td colspan="2"><b style="margin-left:100px; font-size:18px; color:#2d4052;">{{strtoupper($order->mode_of_payment)}}</b></td>
            </tr>
            <tr>
                    <td><b style="font-size:18px; color:#36ade2;">Subtotal </b></td>
                    <td><b style="margin-left:100px; font-size:18px; color:#2d4052;">{{$order->subtotal}} INR</b></td>
              </tr>
              <tr>
                <td><b style="font-size:18px; color:#36ade2;">Shipping</b></td>
                <td><b style="margin-left:100px; font-size:18px; color:#2d4052;">{{$order->shipping}} INR</b></td>
          </tr>
          @if($order->coupon)
          <tr>
            <td><b style="font-size:18px; color:#36ade2;">Discount</b></td>
            <td><b style="margin-left:100px; font-size:18px; color:#2d4052;">-{{$order->discount}} INR</b></td>
        </tr>
          @endif
          <tr>
            <td><b style="font-size:18px; color:#36ade2;">Payable Amount</b></td>
            <td><b style="margin-left:100px; font-size:18px; color:#2d4052;">{{$order->payable_amount}} INR</b></td>
        </tr>
        </table>
    
    </center>
@endsection