<div class='btn-group btn-group-sm'>
    
    <a onclick="trashUtil(this)" trash-url="{{ $url }}" trash-item-to-remove="tr" data-toggle="tooltip" data-placement="bottom" title="Delete" href="javascript:void(0)" class='btn btn-link'>
      <i class="fa fa-trash"></i>
    </a>
  </div>
  