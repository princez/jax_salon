<div class="row">
    {!! csrf_field() !!}

    <div class="col-md-4 col-sm-4 form-group">
        <label>Category</label>
        <select name="parent_id" id="" class="form-control">
            @foreach($categories as $category)
                <option {{$category->id==old('parent_id',$Data->parent_id??null)?'selected':''}} value="{{$category->id}}">{{$category->title}}</option>
            @endforeach
        </select>
        <p class="error">
            {{$errors->first('parent_id')}}
        </p>
    </div>
    <div class="col-md-4 col-sm-4 form-group">
        <label>Title</label>
        <input type="text" name="title"  class="form-control" value="{{old('title',$Data->title??'')}}" />
        <p class="error">
            {{$errors->first('title')}}
        </p>
    </div>

    <div class="col-md-4 col-sm-4 form-group">
        <label>Icon</label>
        <input type="file" name="icon"  class="form-control" value="{{old('icon',$Data->icon??'')}}" />
        <p class="error">
            {{$errors->first('icon')}}
        </p>
    </div>

    

    

   

    
   

  
    
</div>