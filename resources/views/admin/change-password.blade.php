@extends('admin.layouts.default')
@section('title','Change Password')
@section('header')
<div class="container-fluid">
  <div class="header-body">
    <div class="row align-items-center py-4">
      <div class="col-lg-12 col-12">
      <h6 class="h2 text-white d-inline-block mb-0">Change Password</h6> 
      </div>
    </div>
    <!-- Card stats -->
    
  </div>
</div>
@endsection
@section('content')
<div class="row">
  <div class="col-xl-12">
    <div class="card">
        <div class="card-body table-responsive">
            <form method="post"  enctype="multipart/form-data">
                {!! csrf_field() !!}
                <div class="row">
                    <div class="col-md-4 col-sm-4 form-group">
                        <label for="">Current Password</label>
                        <input type="password" name="current_password" class="form-control">
                        <p class="error">
                            {{$errors->first('current_password')}}
                        </p>
                    </div>
                    <div class="col-md-4 col-sm-4 form-group">
                        <label for="">New Password</label>
                        <input type="password" name="new_password" class="form-control">
                        <p class="error">
                            {{$errors->first('new_password')}}
                        </p>
                    </div>
                    <div class="col-md-4 col-sm-4 form-group">
                        <label for="">Confirm New Password</label>
                        <input type="password" name="new_password_confirmation" class="form-control">
                        <p class="error">
                            {{$errors->first('new_password_confirmation')}}
                        </p>
                    </div>
                </div>
                <div class="row">
                  <div class="col-md-12 form-group">
                    <button type="submit" class="btn btn-default float-right">Update Password</button>
                  </div>
                </div>
            </form>
        </div>
    </div>
  </div>
  
</div>
@endsection
@push('scripts')

@endpush