@php $ROUTE = Route::currentRouteName(); 

@endphp
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
 
  <title>{{env('APP_NAME')}}-@yield('title')</title>
  
  @include('admin.layouts.partials.links')
  <style>
    .select2-results__option.select2-results__option--highlighted{
      background: #5e72e4!important;
      color:white!important;
    }
  </style>
</head>

<body>
  <!-- Sidenav -->
  @include('admin.layouts.partials.sidenav')
  <!-- Main content -->
  <div class="main-content" id="panel">
    <!-- Topnav -->
   @include('admin.layouts.partials.topnav')
    <!-- Header -->
    <!-- Header -->
    <div class="header bg-primary pb-6">
        @yield('header')
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">
      @yield('content')
     
      <!-- Footer -->
      <footer class="footer pt-0">
        <div class="row align-items-center justify-content-lg-between">
          <div class="col-lg-6">
            <div class="copyright text-center  text-lg-left  text-muted">
                &copy; 2020 <a href="#" class="font-weight-bold ml-1" target="_blank">{{env('APP_NAME')}}</a>
            </div>
          </div>
          <div class="col-lg-6">
           
          </div>
        </div>
      </footer>
    </div>
  </div>
  <div class="modal" id="myModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Modal title</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <p>Modal body text goes here.</p>
        </div>
        <div class="modal-footer">
         
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
 @include('admin.layouts.partials.templates')
 @include('admin.layouts.partials.scripts')

</body>

</html>
 