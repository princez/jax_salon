 <script>
     const __URL = "{{url("")}}";
     const Notification = ()=>{
        @if(Session::get('alert-message'))
          @if(Session::get('alert-toast'))
            TOAST("{{Session::get('alert-message')}}","{{Session::get('alert-icon')}}")
          @else
            WARN("{{Session::get('alert-message')}}","{{Session::get('alert-icon')}}")
          @endif
        @endif
    }
 </script>
 <!-- Argon Scripts -->
  <!-- Core -->
  <script src="{{url('assets/vendor/jquery/dist/jquery.min.js')}}"></script>
  <script src="{{url('assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js')}}"></script>
  <script src="{{url('assets/vendor/js-cookie/js.cookie.js')}}"></script>
  <script src="{{url('assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js')}}"></script>
  <script src="{{url('assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js')}}"></script>
  <script src="{{url('assets/vendor/sweetalert2/js.js')}}"></script>
  <script type="text/javascript" src="{{url('assets/vendor/datatables/jquery.dataTables.js')}}"></script>
  <script type="text/javascript" src="{{url('assets/vendor/datatables/dataTables.bootstrap4.js')}}"></script>
  <script type="text/javascript" src="{{url('assets/vendor/datatables/buttons/dataTables.buttons.min.js')}}"></script>
  <script type="text/javascript" src="{{url('assets/vendor/datatables/buttons.js') }}"></script>
  <script type="text/javascript" src="{{url('assets/vendor/datatables/buttons/buttons.colVis.min.js')}}"></script>
  <script type="text/javascript" src="{{ url('assets/vendor/datatables/buttons.server-side.js') }}"></script>             
  <!-- Optional JS -->
  <script type="text/javascript" src="{{ asset('https://cdn.datatables.net/colreorder/1.5.0/js/dataTables.colReorder.js') }}"></script>
  <script type="text/javascript" src="{{ asset('https://cdn.datatables.net/responsive/2.2.2/js/dataTables.responsive.js') }}"></script>
  <script type="text/javascript" src="{{ asset('https://cdn.datatables.net/rowgroup/1.0.3/js/dataTables.rowGroup.js') }}"></script>
  <script src="{{url('assets/vendor/chart.js/dist/Chart.min.js')}}"></script>
  <script src="{{url('assets/vendor/select2/dist/js/select2.min.js')}}"></script>

  <script src="{{url('assets/vendor/chart.js/dist/Chart.extension.js')}}"></script>
  <!-- Argon JS -->
  <script src="{{url('assets/js/argon.js?v=1.2.0')}}"></script>
  <script src="{{url('assets/js/utils.js')}}"></script>
  <script src="{{url('assets/js/custom.js')}}"></script>
  @stack('scripts')