 <meta name="csrf-token" content="{{ csrf_token() }}">
  <!-- Favicon -->
  <link rel="icon" href="{{url('assets/img/brand/favicon.png')}}" type="image/png">
  <!-- Fonts -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
  <!-- Icons -->
  <link rel="stylesheet" href="{{url('assets/vendor/nucleo/css/nucleo.css')}}" type="text/css">
  
  <link rel="stylesheet" href="{{url('assets/vendor/sweetalert2/css.css')}}" type="text/css">

  <link rel="stylesheet" href="{{url('assets/vendor/select2/dist/css/select2.min.css')}}" type="text/css">

  

  <link rel="stylesheet" href="{{url('assets/vendor/@fortawesome/fontawesome-free/css/all.min.css')}}" type="text/css">
  <!-- Page plugins -->
  <link rel="stylesheet" href="{{url('assets/vendor/datatables/dataTables.bootstrap4.css')}}">
  <!-- Argon CSS -->
  <link rel="stylesheet" href="{{url('assets/css/argon.css?v=1.2.0')}}" type="text/css">

  <link rel="stylesheet" href="{{url('assets/css/style.css')}}" type="text/css">