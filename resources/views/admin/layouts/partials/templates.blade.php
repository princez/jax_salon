<template name="reasonForm">
    <form action="" method="POST">
        <div class="row">
           {!! csrf_field() !!}
            <div class="col-md-12 form-group">
                <label for="">Reason(Optional)</label>
                <textarea placeholder="Reason for failing the order . If order has been paid online ...enter refund transaction id here " name="reason" class="form-control" ></textarea>
            </div>
            <div class="col-md-12 form-group">
                <button type="submit" class="btn btn-danger rounded float-right">Update Fail</button>
            </div>
        </div>
    </form>
</template>