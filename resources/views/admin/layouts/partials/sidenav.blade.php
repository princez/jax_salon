@php 
  $routes =[
    [
      'title'=>"Dashboard",
      'icon'=>"ni ni-tv-2 text-primary",
      'route'=>'admin.home',
      'match'=>'admin.home'
   ],
   
   [
      'title'=>"Users",
      'icon'=>"ni ni-circle-08 text-primary",
      'route'=>'admin.users.index',
      'match'=>'admin.users.index'
   ],
   
   [
      'title'=>"Categories",
      'icon'=>"ni ni-bullet-list-67 text-success",
      'route'=>'admin.categories.index',
      'match'=>'admin.categories.index'
   ],

   [
      'title'=>"Headings",
      'icon'=>"ni ni-bullet-list-67 text-success",
      'route'=>'admin.headings.index',
      'match'=>'admin.headings.index'
   ],

   [
      'title'=>"Services",
      'icon'=>"ni ni-bullet-list-67 text-success",
      'route'=>'admin.services.index',
      'match'=>'admin.services.index'
   ],

   [
      'title'=>"Packages",
      'icon'=>"ni ni-bullet-list-67 text-success",
      'route'=>'admin.packages.index',
      'match'=>'admin.packages.index'
   ],
   
   
   [
      'title'=>"Daily Offers",
      'icon'=>"ni ni-bullet-list-67 text-success",
      'route'=>'admin.offers.index',
      'match'=>'admin.offers.index'
   ],

   [
      'title'=>"Slider & Combos",
      'icon'=>"ni ni-bullet-list-67 text-success",
      'route'=>'admin.sliders.index',
      'match'=>'admin.sliders.index'
   ],

   [
      'title'=>"Salon Parties",
      'icon'=>"ni ni-bullet-list-67 text-success",
      'route'=>'admin.salonParties',
      'match'=>'admin.salonParties'
   ],

   [
      'title'=>"Franchise Enquiries",
      'icon'=>"ni ni-bullet-list-67 text-success",
      'route'=>'admin.franchise',
      'match'=>'admin.franchise'
   ],

   [
      'title'=>"Contact Us",
      'icon'=>"ni ni-bullet-list-67 text-success",
      'route'=>'admin.contactUs',
      'match'=>'admin.contactUs'
   ],

   [
      'title'=>"Feedbacks",
      'icon'=>"ni ni-bullet-list-67 text-success",
      'route'=>'admin.feedback',
      'match'=>'admin.feedback'
   ],

   [
      'title'=>"Home Services",
      'icon'=>"ni ni-bullet-list-67 text-success",
      'route'=>'admin.homeServices',
      'match'=>'admin.homeServices'
   ],

   [
      'title'=>"Career Enquiries",
      'icon'=>"ni ni-bullet-list-67 text-success",
      'route'=>'admin.career',
      'match'=>'admin.career'
   ],
   

  ];
  
@endphp
<nav class="sidenav navbar navbar-vertical  fixed-left  navbar-expand-xs navbar-light bg-white" id="sidenav-main">
    <div class="scrollbar-inner">
      <!-- Brand -->
      <div class="sidenav-header  align-items-center">
        <a class="navbar-brand" href="javascript:void(0)">
          <h2 style="color:#5e72e4; font-weight:1000;">{{env('APP_NAME')}}</h2>
        </a>
      </div>
      <div class="navbar-inner">
        <!-- Collapse -->
        <div class="collapse navbar-collapse" id="sidenav-collapse-main">
          <!-- Nav items -->
          <ul class="navbar-nav">
            @foreach($routes as $route)
              <li class="nav-item">
              <a class="nav-link {{(Request::is($route['match'].'*') || $ROUTE==$route['match'] )?'active':''}}" href="{{route($route['route'])}}">
                  <i class="{{$route['icon']}}"></i>
                  <span class="nav-link-text">{{$route['title']}}</span>
                </a>
              </li>
            @endforeach
            
           
            <li class="nav-item">
             <a class="nav-link" onclick="logout()" href="javascript:void(0)">
                <i class="ni ni-user-run text-dark"></i>
                <span class="nav-link-text">Logout</span>
              </a>
            </li>
          </ul>
        
         
       
        </div>
      </div>
    </div>
  </nav>