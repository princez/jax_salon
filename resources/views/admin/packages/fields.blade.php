<div class="row">
    {!! csrf_field() !!}

    <div class="col-md-4 col-sm-4 form-group">
        <label>Category</label>
        <select name="category_id" id="" class="form-control">
            @foreach($categories as $category)
                <option {{$category->id==old('category_id',$Data->category_id??null)?'selected':''}} value="{{$category->id}}">{{$category->title}}</option>
            @endforeach
        </select>
        <p class="error">
            {{$errors->first('title')}}
        </p>
    </div>
    <div class="col-md-8 col-sm-8 form-group">
        <label>Title</label>
        <input type="text" name="title"  class="form-control" value="{{old('title',$Data->title??'')}}" />
        <p class="error">
            {{$errors->first('title')}}
        </p>
    </div>

    <div class="col-md-12 col-sm-12 form-group">
        <label>Description</label>
        <textarea name="description"  class="form-control">{{old('description',$Data->description??'')}}</textarea>
        <p class="error">
            {{$errors->first('description')}}
        </p>
    </div>

    <div class="col-md-3 col-sm-3 form-group">
        <label>Image</label>
        <input type="file" name="image"  class="form-control" value="{{old('image',$Data->image??'')}}" />
        <p class="error">
            {{$errors->first('image')}}
        </p>
    </div>

    <div class="col-md-3 col-sm-3 form-group">
        <label>Duration (No. of Days)</label>
        <input type="number" name="duration"  class="form-control" value="{{old('duration',$Data->duration??'')}}" />
        <p class="error">
            {{$errors->first('duration')}}
        </p>
    </div>  

    <div class="col-md-3 col-sm-3 form-group">
        <label>Price</label>
        <input type="text" name="price"  class="form-control" value="{{old('price',$Data->price??'')}}" />
        <p class="error">
            {{$errors->first('price')}}
        </p>
    </div>  

    <div class="col-md-3 col-sm-3 form-group">
        <label>Sale Price</label>
        <input type="text" name="sale_price"  class="form-control" value="{{old('sale_price',$Data->sale_price??'')}}" />
        <p class="error">
            {{$errors->first('sale_price')}}
        </p>
    </div>  

  
    
</div>