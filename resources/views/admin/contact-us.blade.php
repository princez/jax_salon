@extends('admin.layouts.default')
@section('title','Contact Us')
@section('header')
<div class="container-fluid">
  <div class="header-body">
    <div class="row align-items-center py-4">
      <div class="col-lg-12 col-12">
      <h6 class="h2 text-white d-inline-block mb-0">Contact Us</h6> 
      </div>
    </div>
    <!-- Card stats -->
    
  </div>
</div>
@endsection
@section('content')
<div class="row">
  <div class="col-xl-12">
    <div class="card">
        <div class="card-body table-responsive">
            <table class="table d_table table-bordered table-striped">
              <thead>
                <tr>
                  <th>Id</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Phone</th>
                 <th>Message</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody>
               
              </tbody>
            </table>
        </div>
    </div>
  </div>
  
</div>
@endsection
@push('scripts')

<script>
   
</script>
@endpush