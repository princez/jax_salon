@extends('admin.layouts.default')
@section('title','Home')
@section('header')
<div class="container-fluid">
  <div class="header-body">
    <div class="row align-items-center py-4">
      <div class="col-lg-6 col-7">
        <h6 class="h2 text-white d-inline-block mb-0">Home</h6>
        <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
          <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
            <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
            <li class="breadcrumb-item"><a href="#">Home</a></li>
           
          </ol>
        </nav>
      </div>
      
    </div>
    <!-- Card stats -->
   
  </div>
</div>
@endsection
@section('content')
  <div class="row">

    <div class="col-md-4 col-sm-4">
      <div class="card">
        <div class="card-header bg-success text-white text-center">
          Total Users
        </div>
        <div class="card-body text-center">
          <h4>{{$users}}</h4>
        </div>
      </div>
    </div>

    <div class="col-md-4 col-sm-4">
      <div class="card">
        <div class="card-header bg-success text-white text-center">
          Total Categories
        </div>
        <div class="card-body text-center">
          <h4>{{$categories}}</h4>
        </div>
      </div>
    </div>

    <div class="col-md-4 col-sm-4">
      <div class="card">
        <div class="card-header bg-success text-white text-center">
          Total Services
        </div>
        <div class="card-body text-center">
          <h4>{{$services}}</h4>
        </div>
      </div>
    </div>

  </div>
@endsection
@push('scripts')

@endpush