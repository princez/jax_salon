@extends('admin.layouts.default')
@section('title','Customer Transactions')
@section('header')
<div class="container-fluid">
  <div class="header-body">
    <div class="row align-items-center py-4">
      <div class="col-lg-12 col-12">
      <h6 class="h2 text-white d-inline-block mb-0">{{$User->first_name}} Subscriptions</h6> <a href="{{route('admin.users.index')}}" class="btn btn-default float-right"><i class="fa fa-plus"></i>&nbsp;&nbsp;Back To List</a>
      </div>
    </div>
    <!-- Card stats -->
    
  </div>
</div>
@endsection
@section('content')
<div class="row">

    <div class="col-xl-12">
        <div class="card">
            <div class="card-body ">
                <form action="" method="post">
                    @csrf
                    <div class="row"> 
                        <div class="col-md-12">
                            <h2> {{$User->full_name}}</h2>
                        </div>
                        <div class="col-md-4 col-sm-4 form-group">
                            <label for="">Category</label>
                            <select name="category" id="" class="form-control loadDataOnChange" target="[name='package_id']">
                                <option value="">Select</option>
                                @foreach($categories as $category)
                                    <option {{old('category')==$category->id?'selected':""}} value="{{$category->id}}">{{ucfirst($category->title)}}</option>
                                @endforeach
                            </select>
                            <p class="error">
                                {{$errors->first('category')}}
                            </p>
                        </div>

                        <div class="col-md-4 col-sm-4 form-group">
                            <label for="">Package</label>
                            <select name="package_id" id="" url="{{route('admin.ajax.packages')}}" class="form-control">
                                <option value="">Select Category First</option>
                            </select>
                            <p class="error">
                                {{$errors->first('package_id')}}
                            </p>
                        </div>

                      

                        <div class="col-md-12">
                            <button type="submit" class="btn btn-info">Add Subscription</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
      </div>
  
  <div class="col-xl-12">
    <div class="card">
        <div class="card-body ">
          <div class="row">
           
            <div class="col-xs-12 table-responsive">
                <table class="table-bordered table table-striped d_table">
                    <thead>
                        <th>Status</th>
                        <th>Package</th>
                        <th>Expires AT</th>
                        <th>Subscribed At</th>
                    </thead>
                    <tbody>
                        @foreach($subscriptions as $transaction)
                        <tr>
                           <td>
                               @if(!$transaction->has_expired)
                               <span class="badge badge-success">Active</span>
                               @else
                               <span class="badge badge-danger">Expired</span>
                               
                               @endif
                           </td>
                            <td>
                                {{$transaction->package}}
                            </td>
                            <td>
                                {{$transaction->expires_at}}
                            </td>
                            <td>
                                {!! getDateColumn($transaction) !!}
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
          </div>  
          
        </div>
    </div>
  </div>
  
</div>
@endsection
@push('scripts')

<script>
   
</script>
@endpush