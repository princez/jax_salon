@extends('admin.layouts.default')
@section('title','Customer Transactions')
@section('header')
<div class="container-fluid">
  <div class="header-body">
    <div class="row align-items-center py-4">
      <div class="col-lg-12 col-12">
      <h6 class="h2 text-white d-inline-block mb-0">{{$Wallet->first_name}} Transactions</h6> <a href="{{route('admin.users.index')}}" class="btn btn-default float-right"><i class="fa fa-plus"></i>&nbsp;&nbsp;Back To List</a>
      </div>
    </div>
    <!-- Card stats -->
    
  </div>
</div>
@endsection
@section('content')
<div class="row">

    <div class="col-xl-12">
        <div class="card">
            <div class="card-body ">
                <form action="" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-md-12">
                            <h2>Current Balance:- {{$Wallet->wallet}}</h2>
                        </div>
                        <div class="col-md-4 col-sm-4 form-group">
                            <label for="">Type</label>
                            <select name="type" id="" class="form-control">
                                @foreach(['credit','debit'] as $s)
                                    <option {{old('type')==$s?'selected':""}} value="{{$s}}">{{ucfirst($s)}}</option>
                                @endforeach
                            </select>
                            <p class="error">
                                {{$errors->first('type')}}
                            </p>
                        </div>

                        <div class="col-md-4 col-sm-4 form-group">
                            <label for="">Amount</label>
                            <input type="text" name="amount" value="{{old('amount')}}" class="form-control">
                            <p class="error">
                                {{$errors->first('amount')}}
                            </p>
                        </div>

                        <div class="col-md-12">
                            <button type="submit" class="btn btn-info">Add Transaction</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
      </div>
  
  <div class="col-xl-12">
    <div class="card">
        <div class="card-body ">
          <div class="row">
           
            <div class="col-xs-12 table-responsive">
                <table class="table-bordered table table-striped d_table">
                    <thead>
                        <th>Id</th>
                        <th>Type</th>
                        <th>Amount</th>
                        <th>Time</th>
                    </thead>
                    <tbody>
                        @foreach($transactions as $transaction)
                        <tr>
                            <td>
                                {{$transaction->id}}
                            </td>
                            <td>
                                {{$transaction->type}}
                            </td>
                            <td>
                                {{$transaction->amount}}
                            </td>
                            <td>
                                {!! getDateColumn($transaction) !!}
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
          </div>  
          
        </div>
    </div>
  </div>
  
</div>
@endsection
@push('scripts')

<script>
   
</script>
@endpush