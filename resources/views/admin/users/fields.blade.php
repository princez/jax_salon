<div class="row">
    {!! csrf_field() !!}

    <div class="col-md-4  col-sm-4 form-group">
        <label>Role</label>
        <select name="role" id="" class="form-control">
          @foreach(['user'] as $u)
            <option {{old('role',$User->role??null)==$u?'selected':''}} value="{{$u}}">{{ucfirst($u)}}</option>
          @endforeach
        </select>
        <p class="error">
            {{$errors->first('role')}}
        </p>
    </div>
  
    <div class="col-md-4  col-sm-4 form-group">
        <label>Name</label>
        <input type="text" name="full_name"  class="form-control" value="{{old('full_name',$User->full_name??'')}}" />
        <p class="error">
            {{$errors->first('full_name')}}
        </p>
    </div>

    <div hidden class="col-md-4 col-sm-4  form-group">
        <label>Profile</label>
        <input type="file" name="profile" class="form-control" />
        <p class="error">
            {{$errors->first('profile')}}
        </p>
    </div>
    <div class="col-md-4 col-sm-4 form-group">
        <label>Mobile</label>
        <input type="text" name="mobile"  class="form-control" value="{{old('mobile',$User->mobile??'')}}" />
        <p class="error">
            {{$errors->first('mobile')}}
        </p>
    </div>

    <div class="col-md-4 col-sm-4 form-group">
        <label>Alternate Mobile</label>
        <input type="text" name="alternate_mobile"  class="form-control" value="{{old('alternate_mobile',$User->alternate_mobile??'')}}" />
        <p class="error">
            {{$errors->first('alternate_mobile')}}
        </p>
    </div>

    <div class="col-md-4 col-sm-4 form-group">
        <label>Email</label>
        <input type="email" name="email"  class="form-control" value="{{old('email',$User->email??'')}}" />
        <p class="error">
            {{$errors->first('email')}}
        </p>
    </div>
    <div  class="col-md-4 col-sm-4 form-group">
        <label>Password</label>
        <input type="password" name="password"  class="form-control"  />
        <p class="error">
            {{$errors->first('password')}}
        </p>
    </div>
    
</div>