<div class='btn-group btn-group-sm'>
   
  
  @if($is_banned)
  
  <a data-toggle="tooltip" data-placement="bottom" title="Unsuspend" href="{{ route('admin.users.ban',['id'=>$id,'status'=>0]) }}" class='btn btn-success btn-sm rounded-pill'>
    Unban
  </a>
  @else
  <a data-toggle="tooltip" data-placement="bottom" title="Suspend" href="{{ route('admin.users.ban', ['id'=>$id,'status'=>1]) }}" class='btn btn-danger btn-sm rounded-pill'>
    Ban
  </a>
  @endif 

  
  
</div>
