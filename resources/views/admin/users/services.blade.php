@extends('admin.layouts.default')
@section('title','Customer Transactions')
@section('header')
<div class="container-fluid">
  <div class="header-body">
    <div class="row align-items-center py-4">
      <div class="col-lg-12 col-12">
      <h6 class="h2 text-white d-inline-block mb-0">{{$Wallet->first_name}} Service Transactions</h6> <a href="{{route('admin.users.index')}}" class="btn btn-default float-right"><i class="fa fa-plus"></i>&nbsp;&nbsp;Back To List</a>
      </div>
    </div>
    <!-- Card stats -->
    
  </div>
</div>
@endsection
@section('content')
<div class="row">

    <div class="col-xl-12">
        <div class="card">
            <div class="card-body ">
                <form action="" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-md-12">
                           
                        </div>
                        <div class="col-md-4 col-sm-4 form-group">
                            <label>Parent Category</label>
                            <select name="parent_id" id="" class="form-control loadDataOnChange" target="[name='category_id']">
                                <option value="">Select</option>
                                @foreach($categories as $category)
                                    <option {{$category->id==old('parent_id',$Data->parent_id??null)?'selected':''}} value="{{$category->id}}">{{$category->title}}</option>
                                @endforeach
                            </select>
                            <p class="error">
                                {{$errors->first('parent_id')}}
                            </p>
                        </div>
                        <div class="col-md-4 col-sm-4 form-group">
                            <label>Category</label>
                            <select name="category_id" required url="{{route('admin.ajax.getCategories')}}" target="[name='service_id']" id="" class="form-control loadDataOnChange">
                               
                            </select>
                            <p class="error">
                                {{$errors->first('category_id')}}
                            </p>
                        </div>

                        <div class="col-md-4 col-sm-4 form-group">
                            <label>Service</label>
                            <select name="service_id" url="{{route('admin.ajax.getServices')}}" id="" class="form-control">
                               
                            </select>
                            <p class="error">
                                {{$errors->first('service_id')}}
                            </p>
                        </div>

                        <div class="col-md-12">
                            <button type="submit" class="btn btn-info">Add Service</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
      </div>
  
  <div class="col-xl-12">
    <div class="card">
        <div class="card-body ">
          <div class="row">
           
            <div class="col-xs-12 table-responsive">
                <table class="table-bordered table table-striped d_table">
                    <thead>
                        <th>Id</th>
                        <th>Service</th>
                      
                        <th>Time</th>
                    </thead>
                    <tbody>
                        @foreach($transactions as $transaction)
                        <tr>
                            <td>
                                {{$transaction->id}}
                            </td>
                            <td>
                                {{$transaction->service}}
                            </td>
                          
                            <td>
                                {!! getDateColumn($transaction) !!}
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
          </div>  
          
        </div>
    </div>
  </div>
  
</div>
@endsection
@push('scripts')

<script>
   
</script>
@endpush