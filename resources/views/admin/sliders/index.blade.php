@extends('admin.layouts.default')
@section('title','sliders')
@section('header')
<div class="container-fluid">
  <div class="header-body">
    <div class="row align-items-center py-4">
      <div class="col-lg-12 col-12">
      <h6 class="h2 text-white d-inline-block mb-0">Sliders & Combos</h6> <a href="{{route('admin.sliders.create')}}" class="btn btn-default float-right"><i class="fa fa-plus"></i>&nbsp;&nbsp;Create </a>
      </div>
    </div>
    <!-- Card stats -->
    
  </div>
</div>
@endsection
@section('content')
<div class="row">
  <div class="col-xl-12">
    <div class="card">
        <div class="card-body table-responsive">
            <table class="table d_table table-bordered table-striped">
              <thead>
                <tr>
                  <th>Id</th>
                  <th> Category</th>
                  <th>Slider / Combo</th>
                  <th>Image</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody>
                @foreach($data as $d)
                <tr>
                  <td>
                    {{$d->id}}
                  </td>
                  <td>
                    {{$d->category}}
                  </td>
                 
                  <td>
                    {{$d->type}}
                  </td>
                  <td>
                    <img src="{{$d->image}}" style="max-height:10vh;"/>
                  </td>
              

                  
                  
                  <td>
                    <div class='btn-group btn-group-sm'>
                      {{-- <a data-toggle="tooltip" data-placement="bottom" title="Edit" href="{{ route('admin.sliders.edit', $d->id) }}" class='btn btn-link'>
                        <i class="fa fa-edit"></i>
                      </a> --}}
                      <a onclick="trashUtil(this)" trash-url="{{ route('admin.sliders.destroy', $d->id) }}" trash-item-to-remove="tr" data-toggle="tooltip" data-placement="bottom" title="Delete" href="javascript:void(0)" class='btn btn-link'>
                        <i class="fa fa-trash"></i>
                      </a>
                    </div>
                    
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
        </div>
    </div>
  </div>
  
</div>
@endsection
@push('scripts')

<script>
   
</script>
@endpush