<div class="row">
    {!! csrf_field() !!}

    <div class="col-md-4 col-sm-4 form-group">
        <label>Category</label>
        <select name="category_id" id="" class="form-control">
            @foreach($categories as $category)
                <option {{$category->id==old('category_id',$Data->category_id??null)?'selected':''}} value="{{$category->id}}">{{$category->title}}</option>
            @endforeach
        </select>
        <p class="error">
            {{$errors->first('category_id')}}
        </p>
    </div>

    <div class="col-md-4 col-sm-4 form-group">
        <label>Type</label>
        <select name="type" id="" class="form-control">
            @foreach(["slider","combo"] as $category)
                <option {{$category==old('type',$Data->type??null)?'selected':''}} value="{{$category}}">{{ucfirst($category)}}</option>
            @endforeach
        </select>
        <p class="error">
            {{$errors->first('type')}}
        </p>
    </div>
    

    <div class="col-md-4 col-sm-4 form-group">
        <label>Image</label>
        <input type="file" name="image"  class="form-control" />
        <p class="error">
            {{$errors->first('image')}}
        </p>
    </div>

    

    

   

    
   

  
    
</div>