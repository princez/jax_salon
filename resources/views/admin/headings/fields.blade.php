<div class="row">
    {!! csrf_field() !!}

    <div class="col-md-4 col-sm-4 form-group">
        <label>Gender</label>
        <select name="parent_id" previousValue="{{old('parent_id',$Data->parent_id??null)}}" id="" target="[name='category_id']" class="form-control loadDataOnChange">
        <option value="">Select</option>
            @foreach($categories as $category)
                <option {{$category->id==old('parent_id',$Data->parent_id??null)?'selected':''}} value="{{$category->id}}">{{$category->title}}</option>
            @endforeach
        </select>
        <p class="error">
            {{$errors->first('parent_id')}}
        </p>
    </div>

    <div class="col-md-4 col-sm-4 form-group">
        <label>Category </label>
        <select name="category_id" previousValue="{{old('category_id',$Data->category_id??null)}}" url="{{route('admin.ajax.getCategories')}}" id="" class="form-control">
           
        </select>
        <p class="error">
            {{$errors->first('category_id')}}
        </p>
    </div>
    <div class="col-md-4 col-sm-4 form-group">
        <label>Title</label>
        <input type="text" name="title"  class="form-control" value="{{old('title',$Data->title??'')}}" />
        <p class="error">
            {{$errors->first('title')}}
        </p>
    </div>

    

    

    

   

    
   

  
    
</div>