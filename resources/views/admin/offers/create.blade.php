@extends('admin.layouts.default')
@section('title','Create offer')
@section('header')
<div class="container-fluid">
  <div class="header-body">
    <div class="row align-items-center py-4">
      <div class="col-lg-12 col-12">
      <h6 class="h2 text-white d-inline-block mb-0">Create offer</h6> <a href="{{route('admin.offers.index')}}" class="btn btn-default float-right"><i class="fa fa-arrow-left"></i>&nbsp;Back to offers</a>
      </div>
    </div>
    <!-- Card stats -->
    
  </div>
</div>
@endsection
@section('content')
<div class="row">
  <div class="col-xl-12">
    <div class="card">
        <div class="card-body table-responsive">
            <form method="post" action="{{route('admin.offers.store')}}" enctype="multipart/form-data">
                @include('admin.offers.fields')
                <div class="row">
                  <div class="col-md-12 form-group">
                    <button type="submit" class="btn btn-info float-right">Create </button>
                  </div>
                </div>
            </form>
        </div>
    </div>
  </div>
  
</div>
@endsection
@push('scripts')

@endpush