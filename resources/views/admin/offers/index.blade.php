@extends('admin.layouts.default')
@section('title','offers')
@section('header')
<div class="container-fluid">
  <div class="header-body">
    <div class="row align-items-center py-4">
      <div class="col-lg-12 col-12">
      <h6 class="h2 text-white d-inline-block mb-0">offers</h6> <a href="{{route('admin.offers.create')}}" class="btn btn-default float-right"><i class="fa fa-plus"></i>&nbsp;&nbsp;Create </a>
      </div>
    </div>
    <!-- Card stats -->
    
  </div>
</div>
@endsection
@section('content')
<div class="row">
  <div class="col-xl-12">
    <div class="card">
        <div class="card-body table-responsive">
            <table class="table d_table table-bordered table-striped">
              <thead>
                <tr>
                  <th>Id</th>
                  <th>Category</th>
                  <th>Name</th>
                  <th>Image</th>
                  <th>Duration</th>
                  <th>Price</th>
                  <th>Sale Price</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody>
                @foreach($data as $d)
                <tr>
                  <td>
                    {{$d->id}}
                  </td>
                  <td>
                    {{$d->category}}
                  </td>
                  <td>
                    {{$d->title}}
                  </td>
                  <td>
                    <img src="{{$d->image}}" style="max-height:10vh;" alt="">
                  </td>
                  <td>
                    {{$d->duration}}
                  </td>
                  <td>
                    {{$d->price}}
                  </td>

                  <td>
                    {{$d->sale_price}}
                  </td>

                  
                  
                  <td>
                    <div class='btn-group btn-group-sm'>
                      <a data-toggle="tooltip" data-placement="bottom" title="Edit" href="{{ route('admin.offers.edit', $d->id) }}" class='btn btn-link'>
                        <i class="fa fa-edit"></i>
                      </a>
                      <a onclick="trashUtil(this)" trash-url="{{ route('admin.offers.destroy', $d->id) }}" trash-item-to-remove="tr" data-toggle="tooltip" data-placement="bottom" title="Delete" href="javascript:void(0)" class='btn btn-link'>
                        <i class="fa fa-trash"></i>
                      </a>
                    </div>
                    
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
        </div>
    </div>
  </div>
  
</div>
@endsection
@push('scripts')

<script>
   
</script>
@endpush