<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CareerQuery extends Model
{
    //
    protected $fillable =[
        'first_name',
        'last_name',
        'email',
        'mobile',
        'qualifications',
        'file'
    ];
}
