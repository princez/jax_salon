<?php

namespace App\DataTables;

use App\Party;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class PartiesDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('action', function($obj){
                return view('admin.dataTable-actions',['id'=>$obj->id,'url'=>route('admin.table.destroy',['table'=>'parties','id'=>$obj->id])]);
            })
            
            ->editColumn('updated_at',function($obj){
                return getDateColumn($obj);
            })
            ->editColumn('party_date',function($obj){
                return \DateTime::createFromFormat('Y-m-d',$obj->party_date)->format("d, M y");
            })
            ->editColumn('party_time',function($obj){
                return \DateTime::createFromFormat('H:i:s',$obj->party_time)->format("h:i A");
            })
           
          
            ->rawColumns(['updated_at','action','file']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Party $model)
    {
        return $model
                    ->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    { 
        return $this->builder()
        ->setTableId('franchise-table')
        ->columns($this->getColumns())
        ->minifiedAjax()
        ->dom('Bfrtip')
        ->orderBy(7)
       
        ->buttons(
            // Button::make('create2'),
            // Button::make('export'),
            Button::make('print'),
            Button::make('reset'),
            Button::make('reload')
        );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
                // Column::make('profile')
                // ->orderable(false)
                // ->printable(false),
                Column::make('full_name'),
                Column::make('mobile'),
               
                Column::make('mobile')
                    ->orderable(false),
                Column::make('email'),
                Column::make('no_of_persons'),
                Column::make('party_date'),
                Column::make('party_time'),
                Column::make('updated_at'),
                Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->width(60)
                ->addClass('text-center')
           
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Users_' . date('YmdHis');
    }
}
