<?php

namespace App\DataTables;

use App\CareerQuery;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class CareersDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('action', function($obj){
                return view('admin.dataTable-actions',['id'=>$obj->id,'url'=>route('admin.table.destroy',['table'=>'career_queries','id'=>$obj->id])]);
            })
            
            ->editColumn('updated_at',function($obj){
                return getDateColumn($obj);
            })
            ->editColumn('file',function($obj){
                return "<a href='".url($obj->file)."' class='btn btn-info btn-sm'>Resume</a>";
            })
          
            ->rawColumns(['updated_at','action','file']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(CareerQuery $model)
    {
        return $model
                    ->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    { 
        return $this->builder()
        ->setTableId('franchise-table')
        ->columns($this->getColumns())
        ->minifiedAjax()
        ->dom('Bfrtip')
        ->orderBy(6)
       
        ->buttons(
            // Button::make('create2'),
            // Button::make('export'),
            Button::make('print'),
            Button::make('reset'),
            Button::make('reload')
        );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
                // Column::make('profile')
                // ->orderable(false)
                // ->printable(false),
                Column::make('first_name'),
                Column::make('last_name'),
                Column::make('file')
                ->title("Resume")
                ->searchable(false)
                ->orderable(false),
                Column::make('mobile')
                    ->orderable(false),
                Column::make('email'),
                Column::make('qualifications'),
                Column::make('updated_at'),
                Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->width(60)
                ->addClass('text-center')
           
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Users_' . date('YmdHis');
    }
}
