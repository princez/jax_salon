<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Heading extends Model
{
    //
    protected $fillable = [
        'title',
        'category_id'
    ];

    public function services(){
        return $this->hasMany('App\Service','heading_id','id')
             ->whereType('service')
             ->select(
                 'id',
                 'heading_id',
                 'category_id',
                 'title',
                //  'description',
                //  'image',
                 'duration',
                 'price',
                 'sale_price'
             )
             ;
     }
}
