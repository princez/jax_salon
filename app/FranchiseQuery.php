<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FranchiseQuery extends Model
{
    //
    protected $fillable =[
        'first_name',
        'last_name',
        'email',
        'mobile',
        'message',
        'city'
    ];
}
