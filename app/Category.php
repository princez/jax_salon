<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    protected $fillable = [
        'title',
        'parent_id',
        'icon'
    ];

    public function getIconAttribute($val){
        return url($val);
    }

    public function services(){
       return $this->hasMany('App\Service','category_id','id')
            ->whereType('service')
            ->select(
                'id',
                'category_id',
                'title',
                'description',
                'image',
                'duration',
                'price',
                'sale_price'
            )
            ;
    }

        
}
