<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Otp extends Model
{
    //

    	protected $fillable =[
							'otp',
							'token',
							'email',
							're_sents',
							'attempts',
                            'has_used',
                            'data_id'
						];
	 protected $validFor=600; //in seconds;

    protected $resendWaitTime=60;

    protected $maxAttempts = 10;

    protected $maxResents=10;

    protected $maxSends = 10;
    
    protected $res =['error'=>'Please resend OTP.'];

    protected $_OTP ;					

	 public function init($email,$data_id=null){

	 	$checkCount = Otp::latest()->whereEmail($email)->whereDate('created_at','=',Date('Y-m-d'))->count();

    	if($checkCount>$this->maxSends)
    		return ['error'=>'You already have sent '.$this->maxSends.' Emails  for verification code'];
    	
	 	$check = Otp::latest()->whereEmail($email)->first()->created_at??null;
	 	if($check){
		 	$lastUpdatedTime=$this->getTimeIntervalV2($check);
	        if($lastUpdatedTime<$this->resendWaitTime)
	            return ['error'=>'Please wait for at least '.($this->resendWaitTime-$lastUpdatedTime).' second(s) before sending OTP again.'];
    	}

    	


    	$otp = $this->create([
    							'otp'=>mt_rand(1111111,9999999),
                                'email'=>$email,
                                'data_id'=>$data_id
    	]);

    	$token = uniqid().$otp->id;
    	$temp = clone $otp;
    	$temp->token=$token;
    	$otp->update(['token'=>$token]);
        sendMail($email,"OTP for Email Verification",'mails.text',['text'=>"Your One Time Password is ".$temp->otp]);
    	return $temp;
    }
     public function verifyOtp($token,$otp){
    	$this->_OTP = $this->whereToken($token)->first();

    	if(!$this->_OTP || $this->_OTP->has_used)
    		return $this->res;
    	

    	
    	if($this->getTimeInterval()>$this->validFor)
    		return ['error'=>'The entered OTP is expired','type'=>'otpExpired'];

    	if($this->_OTP->attempts>$this->maxAttempts)
    		return ['error'=>'Max. attempts reached.Please send new verification code and try again !','type'=>'maxAttemptsExceeded'];


    	if($this->_OTP->otp==$otp)
    		return $this;

    	$this->_OTP->increment('attempts');
    	return ['error'=>'The entered OTP is invalid','type'=>'invalidOtp'];
    }

     public function resendOtp($token){
        $this->_OTP = $this->whereToken($token)->first();

        if(!$this->_OTP || $this->_OTP->has_used)
            return $this->res;
        

        
        if($this->getTimeInterval()>$this->validFor)
            return ['error'=>'The OTP request is expired','type'=>'otpExpired'];

        if($this->_OTP->attempts>$this->maxAttempts)
    		return ['error'=>'Max. attempts reached.Please send new verification code and try again !','type'=>'maxAttemptsExceeded'];
        
       
            $lastUpdatedTime=$this->getTimeIntervalV2($this->_OTP->updated_at);
        if($lastUpdatedTime<$this->resendWaitTime)
            return ['error'=>'Please wait for at least '.($this->resendWaitTime-$lastUpdatedTime).' second(s) before sending OTP again.','type'=>'wait'];

        if($this->_OTP->re_sents>$this->maxResents)
            return ['error'=>'Please send new OTP','type'=>'maxResentsExceeded'];

       

        $this->send($this->_OTP->otp,$this->_OTP->email);

        return $this->_OTP->increment('re_sents');
       
    }

    public function getTimeInterval(){
    	$date = new \DateTime(Date('Y-m-d H:i:s'));
		$date2 = new \DateTime($this->_OTP->created_at);

	 return  $date->getTimestamp()-$date2->getTimestamp();
    }

    public function dest(){
    	$this->_OTP->increment('has_used');
    }

    public function getUserId(){
        return $this->_OTP->user_id;
    }


    public function send($otp,$mobile){
        // dd($otp);
        return 12;
        file_get_contents($url);
    }

    public function getTimeIntervalV2($d2){
        $date = new \DateTime(Date('Y-m-d H:i:s'));
        $date2 = new \DateTime($d2);

     return  $date->getTimestamp()-$date2->getTimestamp();
    }

    public function getObject(){
        return $this->_OTP;
    }


}
