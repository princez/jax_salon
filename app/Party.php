<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Party extends Model
{
    //
    protected $fillable = [
        'full_name',
        'email',
        'mobile',
        'no_of_persons',
        'party_date',
        'party_time',
        'user_id',
        'gender'
    ];
}
