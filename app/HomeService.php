<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HomeService extends Model
{
    //
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'mobile',
        'address',
        'gender',
        'services'
    ];
}
