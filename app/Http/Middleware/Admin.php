<?php

namespace App\Http\Middleware;

use Closure;

use \Auth;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $USER = Auth::user();
        if(!$USER || $USER->role!='admin'){
            Auth::logout();
            return redirect()->route('admin.login');
        }

        view()->share(['USER'=>$USER]);

        $request->merge(['USER'=>$USER,'user_id'=>$USER->id]);
                
        return $next($request);
    }
}
