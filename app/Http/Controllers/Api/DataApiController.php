<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Category;
use App\Service;
use App\Heading;
use \Auth;

class DataApiController extends Controller
{
    //
    protected $USER, $USER_ID;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->USER = Auth::user();
            $this->USER_ID = $this->USER->id ?? null;
            return $next($request);
        });
    }

    public function categories()
    {
        $data = Category::whereParentId(null)
            ->select('id', 'title')
            ->get();
        return api()->success(['data' => $data]);
    }

    public function subCategories($parent_id)
    {
        $data = Category::whereParentId($parent_id)
            ->with('services')
            ->select('id', 'title', 'icon')
            ->get();
        return api()->success(['data' => $data]);
    }

    public function services($category_id, Request $req)
    {
        $data = Service::whereType('service')
            ->whereCategoryId($category_id)
            ->join('categories', 'categories.id', '=', 'services.category_id')
            ->select(
                'services.id',
                'services.title',
                'services.heading_id',
                'services.description',
                'services.image',
                'services.duration',
                'services.price',
                'services.sale_price',
                'categories.icon as category_icon'
            );

        if ($req->get('heading_id')) {
            $data->where('services.heading_id', $req->get('heading_id'));
        }
        $data = $data->get();
        if (!count($data)) {
            return api()->notFound(['errorMsg' => 'Services not found !']);
        }

        return api()->success(['data' => $data]);
    }

    public function servicesByHeading($heading_id)
    {
        $data = Service::whereType('service')
            ->where('services.heading_id', $heading_id)
            ->join('categories', 'categories.id', '=', 'services.category_id')
            ->select(
                'services.id',
                'services.title',
                'services.heading_id',
                'services.description',
                'services.image',
                'services.duration',
                'services.price',
                'services.sale_price',
                'categories.icon as category_icon'
            )
            ->orderBy('services.price', 'ASC')
            ->get();
        if (!count($data)) {
            return api()->notFound(['errorMsg' => 'Services not found !']);
        }

        return api()->success(['data' => $data]);
    }

    public function sliders($category_id)
    {
        $data = Service::whereType('slider')
            ->whereCategoryId($category_id)
            ->select('image')
            ->get();

        return api()->success(['data' => $data]);
    }

    public function combos($category_id)
    {
        $data = Service::whereType('combo')
            ->whereCategoryId($category_id)
            ->select('image')
            ->get();

        return api()->success(['data' => $data]);
    }

    public function offers($category_id)
    {
        $data = Service::whereType('offer')
            ->whereCategoryId($category_id)
            ->select(
                'id',
                'title',
                'description',
                'image',
                'price',
                'sale_price'
            )
            ->get();

        return api()->success(['data' => $data]);
    }

    public function packages($category_id)
    {
        $data = Service::whereType('package')
            ->whereCategoryId($category_id)
            ->select(
                'id',
                'title',
                'description',
                'image',
                'duration',
                'price',
                'sale_price'
            )
            ->get();
        if (!count($data)) {
            return api()->notFound(['errorMsg' => 'Packages not found !']);
        }

        return api()->success(['data' => $data]);
    }

    public function servicesByGroup(Request $req)
    {
        $data = Heading::join(
            'categories',
            'categories.id',
            '=',
            'headings.category_id'
        )
            ->join(
                'categories as genders',
                'genders.id',
                '=',
                'categories.parent_id'
            )
            ->with('services')
            ->select(
                'headings.id',
                'genders.title as gender',
                'headings.category_id',
                'headings.title'
            );
        if ($req->get('gender_id')) {
            $data->where('genders.id', $req->get('gender_id'));
        }

        if ($req->get('sub_category_id')) {
            $data->where('categories.id', $req->get('sub_category_id'));
        }

        $data = $data->get();

        // if(!count($data)){
        //     return api()->notFound(['errorMsg'=>'Services not found !']);
        // }

        return api()->success(['data' => $data]);
    }

    public function headings(Request $req)
    {
        $data = Heading::join(
            'categories',
            'categories.id',
            '=',
            'headings.category_id'
        )
            ->join(
                'categories as genders',
                'genders.id',
                '=',
                'categories.parent_id'
            )

            ->select(
                'headings.id',
                'genders.title as gender',
                'headings.category_id',
                'headings.title'
            );
        if ($req->get('gender_id')) {
            $data->where('genders.id', $req->get('gender_id'));
        }

        if ($req->get('sub_category_id')) {
            $data->where('categories.id', $req->get('sub_category_id'));
        }

        $data = $data->get();

        // if(!count($data)){
        //     return api()->notFound(['errorMsg'=>'Services not found !']);
        // }

        return api()->success(['data' => $data]);
    }
}
