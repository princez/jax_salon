<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use \Validator;
use \JWTAuth;
use App\User;

class AuthApiController extends Controller
{
    //
    public function logIn(Request $req){
        $input = Arr::only($req->all(),[
                'email',
                'password'
        ]);

        $rules =[
            'email'=>'required|email',
            'password'=>'required'
        ];

        $validate = Validator::make($input,$rules);
        if($validate->fails()){
            return api()->notValid(['errorMsg'=>$validate->errors()->first()]);
        }
        if(!$token = JWTAuth::attempt($input)){
            return api()->notValid(['errorMsg'=>'The entered credentials are invalid !']);
        }    

        return api()->success(['token'=>$token]);
    }

    public function signUp(Request $req){
        $input = Arr::only($req->all(),[
                'first_name',
                'last_name',
                'mobile',
                'otp_id',
                'otp',
                'email',
                'password'
        ]);

        $rules =[
            'mobile'=>'required|digits_between:8,15',
            'first_name'=>'required|min:2|max:100',
            'last_name'=>'required|min:2|max:100',
            'email'=>'required|min:2|max:200|email|unique:users,email',
            'password'=>'required|min:6|max:15',
            'otp_id'=>'required',
            'otp'=>'required',
        ];

        $errorMessages = [
            'email.unique'=>'The email account is already registered . Please Login !'
        ];

        $validate = Validator::make($input,$rules,$errorMessages);
        if($validate->fails()){
            return api()->notValid(['errorMsg'=>$validate->errors()->first()]);
        }
        $OTP = otp()->verifyOtp($input['otp_id'],$input['otp']);    
        if(!empty($OTP['error'])){
            return api()->notValid(['errorMsg'=>$OTP['error']]);
        }
        if($OTP->getObject()->email!=$input['email']){
            return api()->notValid(['errorMsg'=>'Email verification failed !']);
        }
        $input['role']='user';
        $input['password'] = bcrypt($input['password']);
        $User = User::create($input);
        if(!$User){
            return api()->error();
        }

        if(!$token=JWTAuth::fromUser($User)){
            return api()->error();
        }
         $OTP->dest();    
        return api()->success(['token'=>$token,'message'=>'Account registered successfully !']);
    }

    public function sendVerificationCode(Request $req){
        $input['email']=$req->get('email');
        $rules['email']='required|email';
         $messages = [
            'email.unique'=>'The email account is already registered with us !'
        ];
        $validator = Validator::make($input,$rules,$messages);

        if($validator->fails())
            return api()->notValid(['errorMsg'=>$validator->errors()->first()]);


        $OTP = otp()->init($req->get('email'));

        if(!empty($OTP['error']))
            return api()->notValid(['errorMsg'=>$OTP['error']]);
       
       return api()->success(['token'=>$OTP->token,'message'=>'An otp has been sent to '.$input['email']]); 
    }
}
