<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use \Validator;
use \Auth;
use App\User;
use App\Subscription;
class ProfileApiController extends Controller
{
    //
    protected $USER,$USER_ID;

    public function __construct(){
        $this->middleware(function($request,$next){
            $this->USER = Auth::user();
            $this->USER_ID = $this->USER->id??null;
            return $next($request);
        });
    }
    
    public function index(){
        return api()->success(['data'=>$this->USER->getProfile()]);
    }

    public function update(Request $req){
        $input = Arr::only($req->all(),[
            'first_name',
            'last_name',
            'mobile',
            // 'email',
            'profile'
            
        ]);

        $rules =[
            'first_name'=>'required|min:2|max:200',
            'last_name'=>'required|min:2|max:200',
            'mobile'=>'required|numeric|digits:10|regex:^[6789]\d{9}$^',
            // 'email'=>'required|email',
            'profile'=>'nullable|image|max:3000'
        ];

        $validate = Validator::make($input,$rules);
        if($validate->fails()){
            return api()->notValid(['errorMsg'=>$validate->errors()->first()]);
        }

        if($req->hasFile('profile')){
            $input['profile'] = upload($req->file('profile'),'profiles/');
        }else{
            unset($input['profile']);
        }
            
        if($this->USER->update($input)){
            return api()->success(['message'=>'Profile updated successfully !']);
        }

        return api()->error();
    }

    public function changePassword(Request $req){
        $input = Arr::only($req->all(),[
            'current_password',
            'new_password',
            'new_password_confirmation',
            
        ]);

        $rules =[
            'current_password'=>'required',
                'new_password'=>'required|min:6|max:20|confirmed',
                'new_password_confirmation'=>'required'
        ];

        $validate = Validator::make($input,$rules);
        if($validate->fails()){
            return api()->notValid(['errorMsg'=>$validate->errors()->first()]);
        }

        $password = auth()->user()->password;
        if(! \Hash::check($input['current_password'],$password)){
            return api()->notValid(['errorMsg'=>"The current password is invalid "]);
        }
        $input['password']=bcrypt($input['new_password']);
            
        if($this->USER->update($input)){
            return api()->success(['message'=>'Password updated successfully !']);
        }

        return api()->error();

    }

    public function subscriptions(){
        $data = Subscription::whereUserId($this->USER_ID)
                                ->join("services",'services.id','=','subscriptions.package_id')
                                ->with('package')
                                ->select(
                                    'subscriptions.package_id',
                                    'subscriptions.expires_at',
                                    'subscriptions.created_at as subscribed_on',
                                    \DB::raw('(expires_at < DATE(now())) as has_expired')
                                )
                                ->latest('subscriptions.id')
                                ->paginate();

        return $data;
    }

}
