<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use \Validator;
use \Auth;


class ApiController extends Controller
{
    //
    // 

    protected $USER,$USER_ID;

    public function __construct(){
        $this->middleware(function($request,$next){
            $this->USER = Auth::user();
            $this->USER_ID = $this->USER->id??null;
            return $next($request);
        });
    }

    
    
}
