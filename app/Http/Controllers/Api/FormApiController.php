<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use \Validator;
use \Auth;
use App\Party;
use App\Feedback;
use App\ContactUs;
use App\FranchiseQuery;
use App\CareerQuery;
use App\Service;
use App\HomeService;

class FormApiController extends Controller
{
    //
    protected $USER,$USER_ID;

    public function __construct(){
        $this->middleware(function($request,$next){
            $this->USER = Auth::user();
            $this->USER_ID = $this->USER->id??null;
            return $next($request);
        });
    }

    public function skeleton(Request $req){
        $input = Arr::only($req->all(),[
            'first_name',
            'last_name',
            'mobile',
            
            'profile'
            
        ]);

        $rules =[
            'first_name'=>'required|min:2|max:200',
            'last_name'=>'required|min:2|max:200',
            'mobile'=>'required|numeric|digits:10|regex:^[6789]\d{9}$^',
           
            'profile'=>'nullable|image|max:3000'
        ];

        $validate = Validator::make($input,$rules);
        if($validate->fails()){
            return api()->notValid(['errorMsg'=>$validate->errors()->first()]);
        }

        $s= false;
            
        if($s){
            return api()->success(['message'=>'Profile updated successfully !']);
        }

        return api()->error();  
    }

    public function party(Request $req){
        $input = Arr::only($req->all(),[
            'full_name',
            'email',
            'mobile',
            'no_of_persons',
            'party_date',
            'party_time',
            'gender',
        ]);

        $rules =[
            'full_name'=>'required|min:2|max:200',
            'mobile'=>'required|numeric|digits:10|regex:^[6789]\d{9}$^',
            'email'=>'required|email|max:200',
            'no_of_persons'=>'required|numeric|min:0',
            'party_date'=>'required|date|after:'.Date("Y-m-d"),
            'party_time'=>'required|date_format:H:i',
            'gender'=>'required|in:men,women,both'
        ];

        $validate = Validator::make($input,$rules);
        if($validate->fails()){
            return api()->notValid(['errorMsg'=>$validate->errors()->first()]);
        }
        $input['user_id'] = $this->USER_ID;
        $s = Party::create($input);
            
        if($s){
            return api()->success(['message'=>'Form submitted successfully !']);
        }

        return api()->error();  
    }

    public function homeService(Request $req){
        $input = Arr::only($req->all(),[
            'first_name',
            'last_name',
            'email',
            'mobile',
            'address',
            'gender',
            'services',
        ]);

        $rules =[
            'first_name'=>'required|min:2|max:200',
            'last_name'=>'nullable|min:2|max:200',
            'mobile'=>'required|numeric|digits:10|regex:^[6789]\d{9}$^',
            'email'=>'required|email|max:200',
            'address'=>'required|max:500',
            'gender'=>'required|in:men,women,both',
            'services'=>'required|array|distinct',
            'services.*'=>'required|exists:services,id,type,service'
        ];

        $validate = Validator::make($input,$rules);
        if($validate->fails()){
            return api()->notValid(['errorMsg'=>$validate->errors()->first()]);
        }
        $input['user_id'] = $this->USER_ID;

        $input['services']  = Service::whereIn('id',$input['services'])
                                            ->select('title')
                                            ->get()
                                            ->pluck('title')
                                            ->toArray();
        $input['services'] = implode(",",$input['services']);
        // return $input;
        $s = HomeService::create($input);
            
        if($s){
            return api()->success(['message'=>'Home Service submitted successfully !']);
        }

        return api()->error();  
    }

    public function feedback(Request $req){
        $input = Arr::only($req->all(),[
            'first_name',
            'last_name',
            'email',
            'mobile',
            'feedback'
        ]);

        $rules =[
            'first_name'=>'required|min:2|max:200',
            'last_name'=>'nullable|min:2|max:200',
            'mobile'=>'required|numeric|digits:10|regex:^[6789]\d{9}$^',
            'email'=>'required|email|max:200',
            'feedback'=>'required|max:500'
        ];

        $validate = Validator::make($input,$rules);
        if($validate->fails()){
            return api()->notValid(['errorMsg'=>$validate->errors()->first()]);
        }
        
        $s = Feedback::create($input);
            
        if($s){
            return api()->success(['message'=>'Form submitted successfully !']);
        }

        return api()->error();  
    }

    public function contactUs(Request $req){
        $input = Arr::only($req->all(),[
            'first_name',
            'last_name',
            'email',
            'mobile',
            'message'
        ]);

        $rules =[
            'first_name'=>'required|min:2|max:200',
            'last_name'=>'nullable|min:2|max:200',
            'mobile'=>'required|numeric|digits:10|regex:^[6789]\d{9}$^',
            'email'=>'required|email|max:200',
            'message'=>'required|max:500'
        ];

        $validate = Validator::make($input,$rules);
        if($validate->fails()){
            return api()->notValid(['errorMsg'=>$validate->errors()->first()]);
        }
        
        $s = ContactUs::create($input);
            
        if($s){
            return api()->success(['message'=>'Form submitted successfully !']);
        }

        return api()->error();  
    }

    public function franchiseQuery(Request $req){
        $input = Arr::only($req->all(),[
            'first_name',
            'last_name',
            'email',
            'mobile',
            'message',
            'city'
        ]);

        $rules =[
            'first_name'=>'required|min:2|max:200',
            'last_name'=>'nullable|min:2|max:200',
            'mobile'=>'required|numeric|digits:10|regex:^[6789]\d{9}$^',
            'email'=>'required|email|max:200',
            'message'=>'required|max:500',
            'city'=>'required|string|min:2|max:200'
        ];

        $validate = Validator::make($input,$rules);
        if($validate->fails()){
            return api()->notValid(['errorMsg'=>$validate->errors()->first()]);
        }
        
        $s = FranchiseQuery::create($input);
            
        if($s){
            return api()->success(['message'=>'Form submitted successfully !']);
        }

        return api()->error();  
    }

    public function careerQuery(Request $req){
        $input = Arr::only($req->all(),[
            'first_name',
            'last_name',
            'email',
            'mobile',
            'qualifications',
            'resume'
        ]);

        $rules =[
            'first_name'=>'required|min:2|max:200',
            'last_name'=>'nullable|min:2|max:200',
            'mobile'=>'required|numeric|digits:10|regex:^[6789]\d{9}$^',
            'email'=>'required|email|max:200',
            'qualifications'=>'required|max:500',
            'resume'=>'required|file|max:2000|mimes:pdf,docx,jpg,png'
        ];

        $validate = Validator::make($input,$rules);
        if($validate->fails()){
            return api()->notValid(['errorMsg'=>$validate->errors()->first()]);
        }
        $input['file']=upload($req->file('resume'),'resumes/'); 
        $s = CareerQuery::create($input);
            
        if($s){
            return api()->success(['message'=>'Form submitted successfully !']);
        }

        return api()->error();  
    }

    
}
