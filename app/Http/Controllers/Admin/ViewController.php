<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\DataTables\FranchiseQueriesDataTable;
use App\DataTables\ContactUsDataTable;
use App\DataTables\FeedbacksDataTable;
use App\DataTables\CareersDataTable;
use App\DataTables\PartiesDataTable;
use App\DataTables\HomeServicesDataTable;
use App\Category;
use App\User;
use App\Service;


class ViewController extends Controller
{
    //
    public function home(Request $req){
        // return $req->all();
        $categories = Category::whereNotNull('parent_id')->count();
        $users = User::whereRole('user')->count();
        $services = Service::count();
        return view('admin.home',compact('categories','users','services'));
    }

    public function categories(){
        $categories = Category::all();
        return view('admin.category.index',compact('categories'));
    }

    public function franchise(FranchiseQueriesDataTable $table){
        return $table->render('admin.dataTables',['page_name'=>'Franchise Queries']);
    }

    public function contactUs(ContactUsDataTable $table){
        return $table->render('admin.dataTables',['page_name'=>'Contact Us']);
    }

    public function feedback(FeedbacksDataTable $table){
        return $table->render('admin.dataTables',['page_name'=>'Feedbacks']);
    }

    public function career(CareersDataTable $table){
        return $table->render('admin.dataTables',['page_name'=>'Career Queries']);
    }

    public function party(PartiesDataTable $table){
        return $table->render('admin.dataTables',['page_name'=>'Salon Parties']);
    }

    public function homeServices(HomeServicesDataTable $table){
        return $table->render('admin.dataTables',['page_name'=>'Home Services']);
    }

  

   
}
