<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use App\Category;
use Illuminate\Http\Request;

use App\DataTables\CategoriesDataTable;
use Illuminate\Support\Arr;
use \Validator;

use App\Service;

class ServiceController extends Controller
{
    protected $attributes = [
        'category_id',
        'title',
        // 'description',
        // 'image',
        'duration',
        'price',
        'sale_price',
        'heading_id',
        
    ];

    protected $validationRules =[
        'category_id'=>'required|exists:categories,id',
        'title'=>'required|max:300',
        // 'description'=>'required|max:500',
        // 'image'=>'required|image|max:2000',
        'duration'=>'required|numeric|min:1',
        'price'=>'required|numeric|min:0|greater_than_field:sale_price',
        'sale_price'=>'required|numeric|min:0',
        'heading_id'=>'required'
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = Service::join('categories','categories.id','=','services.category_id')
                            ->leftJoin('headings','headings.id','=','services.heading_id')
                            ->select(
                                'services.*',
                                'headings.title as heading',
                                'categories.title as category'
                            )
                            ->whereType('service')

                            ->get();
        return view('admin.services.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $categories = Category::whereNull('parent_id')->get();
        return view('admin.services.create',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $req)
    {
        $input = Arr::only($req->all(),$this->attributes);
        $rules = $this->validationRules;

        $validate = Validator::make($input,$rules);
        if($validate->fails())
            return redirect()->back()->withErrors($validate)->withInput($req->all());
        
        // $input['image'] = upload($req->file('image'),'services/');
        
       if(Service::create($input)){
           flash()->ok('Service Created Successfully !');
           return redirect()->route('admin.services.index');
       }

       flash()->err();
       return redirect()->back();

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //

        $Data = Service::leftJoin('categories','categories.id','=','services.category_id')
                        ->where('services.id',$id)
                        ->select(
                            'categories.parent_id',
                            'services.*'
                            )
                        ->first();

        if(!$Data){
            return abort(404);
        }
        // return $Data;
        $categories = Category::whereNull('parent_id')->get();
        return view('admin.services.edit',compact('Data','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update($id,Request $req)
    {
        //
        $Service = Service::findOrFail($id);
        $input = Arr::only($req->all(),$this->attributes);
        $rules = $this->validationRules;
        // $rules['image'] = str_replace("required",'nullable',$rules['image']);
        // if($req->hasFile('image')){
        //     $input['image'] = upload($req->file('image'),'services/');
        // }else{
        //     unset($input['image']);
        // }
      
        $validate = Validator::make($input,$rules);
        if($validate->fails())
            return redirect()->back()->withErrors($validate)->withInput($req->all());
        


       if($Service->update($input)){
           flash()->ok('Service Updated Successfully !');
           return redirect()->route('admin.services.index');
       }

       flash()->err();
       return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        // return api()->success(['data'=>Category::findOrFail($id)->delete()]);
        if(Service::findOrFail($id)->delete())
            return api()->success(['message'=>"Service deleted successfully !"]);

        return api()->error();
        // return $id;
    }
}
