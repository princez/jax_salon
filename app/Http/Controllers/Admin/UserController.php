<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use \Validator;

use App\DataTables\UsersDataTable;
use App\User;
use App\Category;
use App\WalletTransaction;
use App\ServiceTransaction;
use App\Service;
use App\Subscription;

class UserController extends Controller
{
    protected $attributes = [
        // 'role',
        'full_name',
        // 'profile',
        'mobile',
        'alternate_mobile',
        'email',
        'role',
        'password'
    ];

    protected $validationRules =[
        'full_name'=>'required|min:2|max:200',
        // 'profile'=>'nullable|image|max:8000',
        'email'=>'nullable|max:200|email',
        'mobile'=>'required|digits:10|regex:^[6789]\d{9}$^|unique:users,mobile,NULL,id,deleted_at,NULL',
        'alternate_mobile'=>'nullable|digits:10|regex:^[6789]\d{9}$^',
        'role'=>'required|in:user,subadmin',
        'password'=>'nullable|min:6|max:20'
    ];   
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(UsersDataTable $User)
    {
        //

        return $User->render('admin.users.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        return view('admin.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $req)
    {
        //
        $input = Arr::only($req->all(),$this->attributes);
        $rules = $this->validationRules;

        $validate = Validator::make($input,$rules);
        if($validate->fails())
            return redirect()->back()->withErrors($validate)->withInput($req->all());
        
        // if($req->hasFile('profile')){
        //     $input['profile']  = upload($req->file('profile'),'profile/');
        // }

        if($req->get('password')){
            $input['password']=bcrypt($input['password']);
        }    
        
        
       if(User::create($input)){
           flash()->ok('User created Successfully !');
           return redirect()->route('users.index');
       }

       flash()->err();
       return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $User = User::findOrFail($id);
        return view('admin.users.edit',compact('User'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $req, $id)
    {
        $User = User::findOrFail($id);
        $input = Arr::only($req->all(),$this->attributes);
        $rules = $this->validationRules;
        // $rules['password']=str_replace('required','nullable',$rules['password']);
        $rules['mobile']="required|digits:10|regex:^[6789]\d{9}$^|unique:users,mobile,$id,id,deleted_at,NULL";
       
        $validate = Validator::make($input,$rules);
        if($validate->fails())
            return redirect()->back()->withErrors($validate)->withInput($req->all());

        if($req->get('password'))
            $input['password'] = bcrypt($input['password']);
        else
            unset($input['password']);        

        // if($req->hasFile('profile'))
        //     $input['profile']  = upload($req->file('profile'),'profile/');
        // else
        //     $input['profile'] = $User->profile;


       if($User->update($input)){
           flash()->ok('Customer Updated Successfully !');
           return redirect()->route('users.index');
       }

       flash()->err();
       return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(User::whereNotIn('role',['admin'])->findOrFail($id)->delete())
            return api()->success(['message'=>"User deleted successfully !"]);

        return api()->error();
    }

    public function approve($id){
        $User = User::whereRole('partner')
                        ->whereIsApproved(0)
                        ->find($id);
        if($User->update(['is_approved'=>1]))
            flash()->ok('Partner approved Successfully !');
        else    
            flash()->err();

            return redirect()->back();
    }

    public function ban($id,$s){
        $User = User::whereNotIn('role',['admin'])->find($id);
        if($User->update(['is_banned'=>$s]))
            flash()->ok('Action completed Successfully !');
        else    
            flash()->err();
            
            return redirect()->back();
    }

    public function wallet($user_id){
        $Wallet = User::findOrFail($user_id);
        $transactions = WalletTransaction::whereUserId($user_id)
                                            ->latest()
                                            ->get();
        return view('admin.users.wallet',compact('Wallet','transactions'));
    }

    public function subscriptions($user_id){
        $User = User::findOrFail($user_id);
        $categories = Category::whereNull("parent_id")
                                ->select(
                                    'id',
                                    'title'
                                )->get();

        $subscriptions = Subscription::whereUserId($user_id)
                                        ->join('services','services.id','=','subscriptions.package_id')
                                        ->select(
                                            'services.title as package',
                                            'services.image',
                                            'subscriptions.*',
                                            \DB::raw('(expires_at < DATE(now())) as has_expired')
                                        )
                                        ->latest('subscriptions.id')
                                        ->get();
                                    // return $subscriptions;                            
                    
        return view('admin.users.subscriptions',compact('User','categories','subscriptions'));
    }

    public function addSubscription($user_id,Request $req){
        $Wallet = User::findOrFail($user_id);
        $input = Arr::only($req->all(),[
            'package_id'
        ]);
        $input['user_id'] = $user_id;
        $rules = [
            'package_id'=>'required|exists:services,id,type,package,deleted_at,NULL',
        ];

        

        $validate = Validator::make($input,$rules);
        if($validate->fails())
            return redirect()->back()->withErrors($validate)->withInput($req->all());
        $Package = Service::find($input['package_id']);
        $d = new \DateTime();
        $d->modify("+ $Package->duration Days");
        $input['expires_at']  = $d->format('Y-m-d');
        //Check if subscription already exists
        $check = Subscription::wherePackageId($Package->id)
                                ->whereUserId($user_id)
                                ->whereDate('expires_at','>',Date('Y-m-d'))
                                ->count();
        if($check){
            flash()->ok('Subscription already exists for this package!');
            return redirect()->back();    
        }

        Subscription::create($input);
        
        flash()->ok('Subscription added sucessfully!');
        return redirect()->back();
       
    }

    public function services($user_id){
        $Wallet = User::findOrFail($user_id);
        $categories = Category::whereNull('parent_id')->get();

        $transactions = ServiceTransaction::whereUserId($user_id)
                                            ->join('services','services.id','=','service_transactions.service_id')
                                            ->latest('service_transactions.id')
                                            ->select(
                                                'services.title as service',
                                                'service_transactions.*'
                                            )
                                            ->get();
        return view('admin.users.services',compact('Wallet','transactions','categories'));
    }

    public function walletAdd($user_id,Request $req){
        $Wallet = User::findOrFail($user_id);
        $input = Arr::only($req->all(),[
            'amount',
            'type'
        ]);
        $input['user_id'] = $user_id;
        $rules = [
            'type'=>'required|in:credit,debit',
            'amount'=>'required|numeric|min:0'
        ];

        if($req->get('type','credit')=='debit'){
            $rules['amount'].='|max:'.$Wallet->wallet;
        }

        $validate = Validator::make($input,$rules);
        if($validate->fails())
            return redirect()->back()->withErrors($validate)->withInput($req->all());

        WalletTransaction::create($input);
        if($input['type']=='credit'){
            User::find($user_id)->increment('wallet',$input['amount']);
        }else{
            User::find($user_id)->decrement('wallet',$input['amount']);
        }

        flash()->ok('Transaction added sucessfully!');
        return redirect()->back();
       
    }

    public function addService($user_id,Request $req){
        $Wallet = User::findOrFail($user_id);
        $input = Arr::only($req->all(),[
            'service_id',
            
        ]);
        $input['user_id'] = $user_id;
        $rules = [
           
            'service_id'=>'required'
        ];

     

        $validate = Validator::make($input,$rules);
        if($validate->fails())
            return redirect()->back()->withErrors($validate)->withInput($req->all());

        ServiceTransaction::create($input);
      

        flash()->ok('Service added sucessfully!');
        return redirect()->back();
       
    }
}
