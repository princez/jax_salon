<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use \Auth,\Hash;
use \Validator;

class LoginController extends Controller
{
    //
    public function index(){
        return view('admin.login');
    }

    public function login(Request $req){
    	
        $input = Arr::only($req->all(),[
                                                 'email',
                                                 'password'
                                             ]);
         $rules = [
                         'email'=>'required|email',
                         'password'=>'required'
         ];
         $input['role']='admin';
         $validate = Validator::make($input,$rules);
         if($validate->fails())
             return redirect()->back()->withErrors($validate)->withInput($req->all());
 
         
         if(!Auth::attempt($input)){
             flash()->e("The entered password is invalid");
             return redirect()->back()->withInput($req->all());
         }
        

         $USER = auth()->user();

         flash()->ok("Welcome ".$USER->full_name,true);
         $route = 'admin.home';
      
         return redirect()->route($route);
 
     }
 
     public function logout(){
         Auth::logout();
         flash()->ok("Account Logged out successfully !");
         return redirect()->route('admin.login');
        // return redirect()->back();
     }

     public function showChangePassword(){
         return view('admin.change-password');
     }
     public function changePassword(Request $req){
    	
        $input = Arr::only($req->all(),[
                                                 'current_password',
                                                 'new_password',
                                                 'new_password_confirmation'
                                             ]);
         $rules = [
                         'current_password'=>'required',
                         'new_password'=>'required|min:6|max:20|string|confirmed',
                         'new_password_confirmation'=>'required|min:6|max:20|string'
         ];
       
         $validate = Validator::make($input,$rules);
         if($validate->fails())
             return redirect()->back()->withErrors($validate)->withInput($req->all());
        
         
        if(!Hash::check($input['current_password'],Auth::user()->password)){
            flash()->err('The current password is incorrect !');
            return redirect()->back();
        }
        if(Auth::user()->update(['password'=>bcrypt($input['new_password'])])){
            flash()->ok("Password updated successfully !");
        }else{
            flash()->err();
        }
 
         return redirect()->back();
 
     }
}
