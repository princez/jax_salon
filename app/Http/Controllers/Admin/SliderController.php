<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use App\Service;
use App\Category;
use Illuminate\Http\Request;

use App\DataTables\CategoriesDataTable;
use Illuminate\Support\Arr;
use \Validator;

class SliderController extends Controller
{
    protected $attributes = ['category_id', 'type', 'image'];

    protected $validationRules = [
        'type' => 'required|in:slider,combo',
        'image' => 'required|image|max:2000',
        'category_id' => 'required',
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = Service::whereIn('type', ['slider', 'combo'])
            ->join('categories', 'categories.id', '=', 'services.category_id')
            ->select('services.*', 'categories.title as category')
            ->get();
        // return $data;
        return view('admin.sliders.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $categories = Category::whereNUll('parent_id')->get();
        return view('admin.sliders.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $req)
    {
        $input = Arr::only($req->all(), $this->attributes);
        $rules = $this->validationRules;

        $validate = Validator::make($input, $rules);
        if ($validate->fails()) {
            return redirect()
                ->back()
                ->withErrors($validate)
                ->withInput($req->all());
        }

        if ($req->hasFile('image')) {
            $input['image'] = upload($req->file('image'), 'slider_and_combos/');
        } else {
            unset($input['image']);
        }

        if (Service::create($input)) {
            flash()->ok('Data Created Successfully !');
            return redirect()->route('admin.sliders.index');
        }

        flash()->err();
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //

        $Data = Category::findOrFail($id);
        $categories = Category::whereNull('parent_id')->get();
        return view('admin.sliders.edit', compact('Data', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $req)
    {
        //
        $Category = Category::findOrFail($id);
        $input = Arr::only($req->all(), $this->attributes);
        $rules = $this->validationRules;

        $rules['icon'] = str_replace('required', 'nullable', $rules['icon']);

        $validate = Validator::make($input, $rules);
        if ($validate->fails()) {
            return redirect()
                ->back()
                ->withErrors($validate)
                ->withInput($req->all());
        }

        if ($req->hasFile('icon')) {
            $input['icon'] = upload($req->file('icon'), 'icons/');
        } else {
            unset($input['icon']);
        }

        if ($Category->update($input)) {
            flash()->ok('Category Updated Successfully !');
            return redirect()->route('admin.categories.index');
        }

        flash()->err();
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        // return 23;
        // return api()->success(['data'=>Category::findOrFail($id)->delete()]);
        if (Service::findOrFail($id)->delete()) {
            return api()->success([
                'message' => 'Slider deleted successfully !',
            ]);
        }

        return api()->error();
        // return $id;
    }
}
