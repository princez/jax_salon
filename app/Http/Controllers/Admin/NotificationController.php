<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use \Validator;

use App\DataTables\NotificationsDataTable;
use App\Helpers\Fcm;
use App\Notification;
use App\Device;

class NotificationController extends Controller
{
    protected $attributes = [
        'title',
        'body',
        'thumbnail',
        'url'
    ];

    protected $validationRules =[
        'title'=>'required|min:2|max:200',
        'body'=>'nullable|max:500',
        'thumbnail'=>'nullable|image|max:2000',
        'url'=>'nullable|url'
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(NotificationsDataTable $table)
    {   
        //
        return $table->render('admin.notifications.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.notifications.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $req)
    {
        $input = Arr::only($req->all(),$this->attributes);
        $rules = $this->validationRules;

        $validate = Validator::make($input,$rules);
        if($validate->fails())
            return redirect()->back()->withErrors($validate)->withInput($req->all());
        
        if($req->hasFile('thumbnail')){
            $input['thumbnail']  = upload($req->file('thumbnail'),'notifications/');
        }else{
            unset($input['thumbnail']);
        }
        
       if($data=Notification::create($input)){
        //    $devices = Device::whereNotNull('fcm_token')->select('fcm_token')->get()->pluck('fcm_token')->toArray();
        //    Fcm::push($devices,['title'=>$data->title,'body'=>$data->body,'notification_type'=>'custom']);
           flash()->ok('Notification broadcasted Successfully !');
           return redirect()->route('admin.notifications.index');
       }
      
       flash()->err();
       return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
