<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Category;
use App\Service;
use App\Heading;

class AjaxController extends Controller
{
    //
    public function getCategories($parent_id){
           $data = Category::whereParentId($parent_id)
                            ->select(
                                'categories.id',
                                'title as name'
                            ) ->get();
            
            return api()->success(['data'=>$data]);
    }

    public function getHeadings($cat_id){
        $data = Heading::whereCategoryId($cat_id)
                         ->select(
                             'id',
                             'title as name'
                         ) ->get();
         
         return api()->success(['data'=>$data]);
   }

    public function getServices($cat_id){
        $data = Service::whereCategoryId($cat_id)
                        ->whereType('service')
                         ->select(
                             'id',
                             'title as name'
                         ) ->get();
         
         return api()->success(['data'=>$data]);
 }

 public function packages($category_id){
    $data = Service::whereType('package')
                    ->whereCategoryId($category_id)
                    ->select(
                        'id',
                        \DB::raw("CONCAT(title,'- Rs.',sale_price,' [',duration,' Days ]') as name"),
                        
                    )
                    ->get();
   

    return api()->success(['data'=>$data]);
}

 public function tableDestroy($table,$id){
     \DB::table($table)->whereId($id)->delete();
    return api()->success(['message'=>'Action completed successfully !']);
 }
}
