<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use App\Heading;
use Illuminate\Http\Request;
use App\Category;
use App\DataTables\CategoriesDataTable;
use Illuminate\Support\Arr;
use \Validator;



class HeadingController extends Controller
{
    protected $attributes = [
        'category_id',
        'title'
    ];

    protected $validationRules =[
        'category_id'=>'required|exists:categories,id',
        'title'=>'required|max:300'
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = Heading::leftJoin('categories','categories.id','=','headings.category_id')
                        ->leftJoin('categories as genders','genders.id','=','categories.parent_id')
                            ->select(
                                'categories.title as category',
                                'genders.title as gender',
                                'headings.*'
                            )->get();
                            // return $data;
        return view('admin.headings.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $categories = Category::whereNull('parent_id')->get();
        return view('admin.headings.create',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $req)
    {
        $input = Arr::only($req->all(),$this->attributes);
        $rules = $this->validationRules;
       
        $validate = Validator::make($input,$rules);
        if($validate->fails())
            return redirect()->back()->withErrors($validate)->withInput($req->all());
        
       
        
       if(Heading::create($input)){
           flash()->ok('Heading Created Successfully !');
           return redirect()->route('admin.headings.index');
       }

       flash()->err();
       return redirect()->back();

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //

        $Data = Heading::leftJoin('categories','categories.id','=','headings.category_id')
                            ->select(
                                'categories.parent_id',
                                'headings.*'
                            )
                            ->where('headings.id',$id)
                            ->first();
        if(!$Data){
            return abort(404);
        }
        // return $Data;
        $categories = Category::whereNull('parent_id')->get();
        return view('admin.headings.edit',compact('Data','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update($id,Request $req)
    {
        //
        $Heading = Heading::findOrFail($id);
        $input = Arr::only($req->all(),$this->attributes);
        $rules = $this->validationRules;
      
      
        $validate = Validator::make($input,$rules);
        if($validate->fails())
            return redirect()->back()->withErrors($validate)->withInput($req->all());
        
       

       if($Heading->update($input)){
           flash()->ok('Heading Updated Successfully !');
           return redirect()->route('admin.headings.index');
       }

       flash()->err();
       return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        // return api()->success(['data'=>Category::findOrFail($id)->delete()]);
        if(Heading::findOrFail($id)->delete())
            return api()->success(['message'=>"Heading deleted successfully !"]);

        return api()->error();
        // return $id;
    }
}
