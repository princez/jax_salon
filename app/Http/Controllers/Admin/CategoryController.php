<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use App\Category;
use Illuminate\Http\Request;

use App\DataTables\CategoriesDataTable;
use Illuminate\Support\Arr;
use \Validator;



class CategoryController extends Controller
{
    protected $attributes = [
        'parent_id',
        'title',
        'icon',
    ];

    protected $validationRules =[
        'parent_id'=>'required|exists:categories,id,parent_id,NULL',
        'title'=>'required|max:300',
        'icon'=>'required|image|max:2000'
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = Category::whereNotNull('categories.parent_id')
                        ->join('categories as parent_category','parent_category.id','=','categories.parent_id')
                        ->select(
                            'categories.*',
                            'parent_category.title as parent'
                        )
                        ->get();
        return view('admin.categories.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $categories = Category::whereNull('parent_id')->get();
        return view('admin.categories.create',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $req)
    {
        $input = Arr::only($req->all(),$this->attributes);
        $rules = $this->validationRules;
       
        $validate = Validator::make($input,$rules);
        if($validate->fails())
            return redirect()->back()->withErrors($validate)->withInput($req->all());
        
       if($req->hasFile('icon')){
            $input['icon'] = upload($req->file('icon'),'icons/');
        }else{
            unset($input['icon']);
        }
        
       if(Category::create($input)){
           flash()->ok('Category Created Successfully !');
           return redirect()->route('admin.categories.index');
       }

       flash()->err();
       return redirect()->back();

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //

        $Data = Category::findOrFail($id);
        $categories = Category::whereNull('parent_id')->get();
        return view('admin.categories.edit',compact('Data','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update($id,Request $req)
    {
        //
        $Category = Category::findOrFail($id);
        $input = Arr::only($req->all(),$this->attributes);
        $rules = $this->validationRules;
       
        $rules['icon'] = str_replace("required",'nullable',$rules['icon']);
      
        $validate = Validator::make($input,$rules);
        if($validate->fails())
            return redirect()->back()->withErrors($validate)->withInput($req->all());
        
        if($req->hasFile('icon')){
            $input['icon'] = upload($req->file('icon'),'icons/');
        }else{
            unset($input['icon']);
        }

       if($Category->update($input)){
           flash()->ok('Category Updated Successfully !');
           return redirect()->route('admin.categories.index');
       }

       flash()->err();
       return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        // return api()->success(['data'=>Category::findOrFail($id)->delete()]);
        if(Category::findOrFail($id)->delete())
            return api()->success(['message'=>"Category deleted successfully !"]);

        return api()->error();
        // return $id;
    }
}
