<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    //
    protected $fillable = [
        'user_id',
        'package_id',
        'expires_at'
    ];

    public function getSubscribedOnAttribute($val){
        return \Carbon\Carbon::parse($val)->format('d,M y');
    }

    public function getExpiresAtAttribute($val){
        return \Carbon\Carbon::createFromFormat('Y-m-d',$val)->format('d,M y');
    }

    public function package(){
        return $this->hasOne('App\Service','id','package_id');
    }
}
