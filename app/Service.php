<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Service extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'category_id',
        'title',
        'description',
        'image',
        'duration',
        'price',
        'sale_price',
        'type',
        'heading_id',
    ];
    protected $hidden = ['deleted_at'];

    public function getImageAttribute($v)
    {
        return url($v);
    }

    public function getCategoryIconAttribute($v)
    {
        return url($v);
    }
}
