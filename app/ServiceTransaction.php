<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceTransaction extends Model
{
    //

    protected $fillable = [
        'service_id',
        'user_id'
    ];
}
