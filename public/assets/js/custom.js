var Modal;
$(document).ready(function(){
	Notification();
	$.ajaxSetup({
	    headers: {
	        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	    }
	});
	$('table.d_table').DataTable();
	$('.select_2').select2();
	loadDataOnChange();
	loadPreviousAjaxData();
	Modal=getModalObject($("#myModal"));
	
	try{
		READY();
	}catch(err){
		console.log(err);
	}
		
});

// var registerForm = ()=>{
// 	Modal.show().md().head("Register Here !!").body($("template#template_register").html());
// }

// var loginForm = ()=>{
// 	Modal.show().md().head("Log In !!").body($("template#template_login").html());
// }

const applyHTMLEditor = (selector=null)=>{
	
	if(!selector)
		selector='textarea.html_editor';

	tinymce.init({selector});
}