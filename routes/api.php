<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'data'], function () {
    Route::get('categories', 'DataApiController@categories');

    Route::get('sub/categories/{parent_id}', 'DataApiController@subCategories');

    Route::get('services/{category_id}', 'DataApiController@services');

    Route::get('sliders/{category_id}', 'DataApiController@sliders');

    Route::get('combos/{category_id}', 'DataApiController@combos');

    Route::get('offers/{category_id}', 'DataApiController@offers');

    // Route::get('services/{category_id}', 'DataApiController@services');

    Route::get('packages/{category_id}', 'DataApiController@packages');

    Route::get('services/v2/group', 'DataApiController@servicesByGroup');

    Route::get('headings', 'DataApiController@headings');

    Route::get(
        'services/by/heading/{heading_id}',
        'DataApiController@servicesByHeading'
    );
});

Route::group(['prefix' => 'form'], function () {
    Route::post('party', 'FormApiController@party');

    Route::post('home/service', 'FormApiController@homeService');

    Route::post('feedback', 'FormApiController@feedback');

    Route::post('contact/us', 'FormApiController@contactUs');

    Route::post('franchise/query', 'FormApiController@franchiseQuery');

    Route::post('career/query', 'FormApiController@careerQuery');

    // Route::post('home/service','FormApiController@homeService');
});

Route::group(['prefix' => 'auth'], function () {
    Route::post('log/in', 'AuthApiController@logIn');

    Route::post('sign/up', 'AuthApiController@signUp');

    Route::post('forget/password', 'AuthApiController@forgetPassword');

    Route::post('send/otp', 'AuthApiController@sendVerificationCode');
});

Route::group(['middleware' => 'jwt'], function () {
    Route::group(['prefix' => 'profile'], function () {
        Route::get('/', 'ProfileApiController@index');

        Route::post('/', 'ProfileApiController@update');

        Route::post('change/password', 'ProfileApiController@changePassword');

        Route::get('subscriptions', 'ProfileApiController@subscriptions');
    });
});
