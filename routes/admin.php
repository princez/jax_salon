<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('login',['as'=>'login','uses'=>"LoginController@index"]);

Route::post('login',['as'=>'login','uses'=>"LoginController@login"]);

Route::group(['middleware'=>'Admin'],function(){

    Route::get('/',['as'=>"home",'uses'=>'ViewController@home']);

    Route::resource('categories','CategoryController');

    Route::resource('headings','HeadingController');
   
    Route::resource('users','UserController');

    Route::resource('services','ServiceController');

    Route::resource('packages','PackageController');

    Route::resource('offers','OfferController');

    Route::resource("sliders",'SliderController');

    Route::get('users/wallet/{user_id}',['as'=>'users.wallet','uses'=>'UserController@wallet']);

    Route::get('users/services/{user_id}',['as'=>'users.services','uses'=>'UserController@services']);

    Route::get('users/services/{user_id}',['as'=>'users.services','uses'=>'UserController@services']);

    Route::post('users/services/{user_id}',['as'=>'users.services','uses'=>'UserController@addService']);

    Route::post('users/wallet/{user_id}',['as'=>'users.wallet','uses'=>'UserController@walletAdd']);

    Route::any('users/ban/{id}/{status}',['as'=>'users.ban','uses'=>'UserController@ban']);

    Route::get('users/subscriptions/{user_id}',['as'=>'users.subscriptions','uses'=>'UserController@subscriptions']);

    Route::post('users/subscriptions/{user_id}',['as'=>'users.subscriptions','uses'=>'UserController@addSubscription']);

    Route::get('logout',['as'=>'logout','uses'=>'LoginController@logout']);

    Route::get('change/password',['as'=>'changePassword','uses'=>'LoginController@showChangePassword']);

    Route::post('change/password',['as'=>'changePassword','uses'=>'LoginController@changePassword']);

    Route::get('franchise',['as'=>'franchise','uses'=>'ViewController@franchise']);

    Route::get('contact/us',['as'=>'contactUs','uses'=>'ViewController@contactUs']);

    Route::get('feedbacks',['as'=>'feedback','uses'=>'ViewController@feedback']);

    Route::get('career',['as'=>'career','uses'=>'ViewController@career']);

    Route::delete('delete/{table}/{id}',['as'=>'table.destroy','uses'=>'AjaxController@tableDestroy']);

    Route::get('salon/parties',['as'=>'salonParties','uses'=>'ViewController@party']);

    Route::get('home/services',['as'=>'homeServices','uses'=>'ViewController@homeServices']);

    Route::group(['prefix'=>'ajax','as'=>'ajax.'],function(){

        Route::any('get/categories/{parent_id?}',['as'=>'getCategories','uses'=>'AjaxController@getCategories']);

        Route::any('get/headings/{category_id?}',['as'=>'getHeadings','uses'=>'AjaxController@getHeadings']);
        
        Route::any('get/services/{category_id?}',['as'=>'getServices','uses'=>'AjaxController@getServices']);

        Route::any('packages/{category_id?}',['as'=>'packages','uses'=>'AjaxController@packages']);

    });
    
    
});




